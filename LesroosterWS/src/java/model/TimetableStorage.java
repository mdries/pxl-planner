/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.sun.jersey.spi.resource.Singleton;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mitch Dries
 */
@Singleton
public class TimetableStorage {
    
    private List<Day> timetable;
    
    public TimetableStorage(){
        Timetable ta = new Timetable();
        try {
            timetable = ta.getTimetable();
        } catch (Exception ex) {
            Logger.getLogger(TimetableStorage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public TimetableStorage(String group, String department, String dates){
        try {
            Timetable ta = new Timetable(group, department, dates);
            timetable = ta.getTimetable();
        } catch (Exception ex) {
            Logger.getLogger(TimetableStorage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<Day> getTimetable(){
        return timetable;
    }
}

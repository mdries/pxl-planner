package model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jsoup.*;
import org.jsoup.Connection.Response;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Timetable {

	public static final int REG_EX_HOUR = 1;
	public static final int REG_EX_TITLE = 2;
	public static final int REG_EX_ROOM = 3;
        
        private String dates, department, group;
        
        public Timetable(){
            this.group = "3AOND";
            this.department = "IT%2Fphl";
            this.dates = "131021131028131021";
        }
        
        public Timetable(String group, String department, String dates){
            this.group = group;
            this.department = department;
            this.dates = dates;
        }
        
        public List<Day> getTimetable() throws Exception{
            ArrayList<Day> timetable = new ArrayList<Day>();
            
            String url = "https://kalender.pxl.be/kalenterit2/index.php?kt=lk&yks=&cluokka="+this.group+"&av="+this.dates+"&guest="+this.department+"&lang=fla&print=netti&outmode=excel_inline";
            
            Connection con = Jsoup.connect(url);
		con.userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:5.0) Gecko/20100101 Firefox/5.0");
		
		Response response1 = null;
		Document doc = null;
		
		try {
                    response1 = con.execute();
                    doc = response1.parse();
		} catch (IOException e1) {
                    throw new Exception(e1);
		}               
                
                for(int i = 0; i < 5; i++){
                    timetable.add(new Day(Integer.toString(i)));
                }
                
                /*Day[] lesrooster = new Day[5];
		for(int i = 0; i < lesrooster.length; i++){
			lesrooster[i] = new Day();
                        
		}*/
		
		int currentDay = 0;
		
		Elements dagen = doc.getElementsByTag("tr"); //eerst de rijen eruit halen
		for(Element les : dagen){
			Elements th = les.getElementsByTag("td"); //uit iedere rij de kolom halen
			for (Element td : th) {
				currentDay++;
				Element span = td.getElementsByTag("span").first(); //per kolom de span eruit halen als er geen span is => null				
				if (span != null) {
					String gegevens = span.toString();
					String t = getGeg(gegevens, "(([01]?[0-9]|2[0-3]):[0-5][0-9])-(([01]?[0-9]|2[0-3]):[0-5][0-9])", REG_EX_HOUR); //de tijd ophalen
					String c = getGeg(gegevens, "</b>(.*)- (HC|PR|wifa)", REG_EX_TITLE); //de titel ophalen
					String r = getGeg(gegevens, "((EB|EA|TE|TF)[0-9][0-9][0-9])", REG_EX_ROOM); //het klaslokaal ophalen
                                        timetable.get(currentDay-1).addClass(new Course(c,r,t));
				}
				if(currentDay == 5) {
					currentDay = 0;
				}
			}
		}
                
                return timetable;
        }
	
		
	//methode om de titel, het lokaal of de tijd uit de span te halen
	public static String getGeg(String title, String patternString, int nr) {
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(title);
		String result = null;
		if (nr == REG_EX_TITLE || nr == REG_EX_ROOM) {
			if(matcher.find()) {
				result =  matcher.group(1);
			}	
		}
		else {		
			if(matcher.find()) {
				result = matcher.group(1) + " - " + matcher.group(3);
			 }
		}
                if(result == null){
			result = "";
		}
		return result;
	}

}
package model;

public class Course{
	
	private String course, room, time;

	
	public Course(String course, String room, String time) {
		super();
		this.course = course;
		this.room = room;
		this.time = time;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	@Override
	public boolean equals(Object o) {
		Course class1 = this;
		Course class2 = (Course) o;
		boolean courseEquals, roomEquals, timeEquals;
		courseEquals = class1.course.equals(class2.course);
		roomEquals = class1.room.equals(class2.room);
		timeEquals = class1.time.equals(class2.time);
		return courseEquals && roomEquals && timeEquals;		
	}

}

package model;

import java.util.ArrayList;
import java.util.Iterator;

public class Day implements Iterable<Course> {
	private ArrayList<Course> courses;
        private String day;

	public Day() {
            this.courses = new ArrayList<Course>();
	}
        
        public Day(String day){
            this.courses = new ArrayList<Course>();
            this.day = day;
        }

	public void addClass(Course class1) {
            if (!this.courses.contains(class1)) {
                this.courses.add(class1);
            }
	}

	public ArrayList<Course> getCourses() {
		return this.courses;
	}
        
        public String getDay(){
            return this.day;
        }

	@Override
	public Iterator<Course> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rest;

import com.sun.jersey.api.core.InjectParam;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import model.Day;
import model.TimetableStorage;

/**
 * REST Web Service
 *
 * @author Mitch Dries
 */
@Path("timetable")
public class Timetable {

    @Context
    private UriInfo context;
    
    @InjectParam
    private TimetableStorage tsStore;

    /**
     * Creates a new instance of Timetable
     */
    public Timetable() {
    }

    /**
     * Retrieves representation of an instance of rest.Timetable
     * @return an instance of Timetable
     */
    @GET
    @Produces("application/json")
    public List<Day> getJson(@DefaultValue("3AOND") @QueryParam("group") String group, 
    @DefaultValue("IT%2Fphl") @QueryParam("department") String department,
    @DefaultValue("131021131028131021") @QueryParam("dates") String dates) {
                
        TimetableStorage ts = new TimetableStorage(group, department, dates);
        return ts.getTimetable();
    }

    /**
     * PUT method for updating or creating an instance of Timetable
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
   /* @PUT
    @Consumes("application/json')
    public void putJson(Timetable content) {
    }*/
}

package be.pxl.planner.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SQLiteHelper extends SQLiteOpenHelper {
	public static final String TABLE_COURSE = "course";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_ROOM = "room";
	public static final String COLUMN_TIME = "time";
	public static final String COLUMN_BEGIN_HOUR = "begin_hour";
	public static final String COLUMN_END_HOUR = "end_hour";
	public static final String COLUMN_DAY = "day";
	
	private static final String DATABASE_NAME = "pxlplanner.db";
	private static final int DATABASE_VERSION = 6;
	
	 private static final String DATABASE_CREATE = "create table "
		      + TABLE_COURSE + "(" + 
			 COLUMN_ID + " integer primary key autoincrement, " + 
		     COLUMN_NAME + " text not null, " +
		     COLUMN_ROOM + " text not null, " +
		     COLUMN_TIME + " text not null, " +
		     COLUMN_BEGIN_HOUR + " integer not null, " +
		     COLUMN_END_HOUR + " integer not null, " +
		     COLUMN_DAY + " integer" +
			 ");";
	 
	 public static final String TABLE_TASK = "task";
	 public static final String TASK_ID = "id";
	 public static final String TASK_NAME = "name";
	 public static final String TASK_DESCRIPTION = "description";
	 public static final String TASK_DEADLINE = "deadline";
	 public static final String TASK_COURSE = "course";
	 public static final String TASK_PERCENTAGE = "percentage";
	 
	 public static final String DATABASE_CREATE_TABLE_TASK = "create table " 
			 + TABLE_TASK + "(" +
	 		TASK_ID + " integer primary key autoincrement, " + 
		    TASK_NAME + " text not null, " +
		    TASK_DESCRIPTION + " text, " +
		    TASK_DEADLINE + " int not null, " +
		    TASK_COURSE + " text not null, " +
		    TASK_PERCENTAGE + " integer" +
			");";
	 
	public SQLiteHelper(Context context, CursorFactory factory) {
		super(context, DATABASE_NAME, factory, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DATABASE_CREATE);
		db.execSQL(DATABASE_CREATE_TABLE_TASK);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	    Log.w(SQLiteHelper.class.getName(),
	            "Upgrading database from version " + oldVersion + " to "
	                + newVersion + ", which will destroy all old data");
	        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COURSE);
	        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TASK);
	        onCreate(db); 

	}

}

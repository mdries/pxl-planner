package be.pxl.planner.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import be.pxl.planner.model.Course;

public class CourseDataSource {

	private SQLiteDatabase database;
	private SQLiteHelper dbHelper;
	private String[] allColumns = { SQLiteHelper.COLUMN_ID,
			SQLiteHelper.COLUMN_NAME, SQLiteHelper.COLUMN_ROOM,
			SQLiteHelper.COLUMN_TIME, SQLiteHelper.COLUMN_BEGIN_HOUR,
			SQLiteHelper.COLUMN_END_HOUR, SQLiteHelper.COLUMN_DAY };

	public CourseDataSource(Context context) {
		dbHelper = new SQLiteHelper(context, null);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}
	
	public Course createCourse(String name, String room, String time, int beginHour, int endHour, int day){
		ContentValues values = new ContentValues();
		values.put(SQLiteHelper.COLUMN_NAME, name);
		values.put(SQLiteHelper.COLUMN_ROOM, room);
		values.put(SQLiteHelper.COLUMN_TIME, time);
		values.put(SQLiteHelper.COLUMN_BEGIN_HOUR, beginHour);
		values.put(SQLiteHelper.COLUMN_END_HOUR, endHour);
		values.put(SQLiteHelper.COLUMN_DAY, day);
		
		long insertId = database.insert(SQLiteHelper.TABLE_COURSE, null, values);
		
		Cursor cursor = database.query(SQLiteHelper.TABLE_COURSE, allColumns, SQLiteHelper.COLUMN_ID + " = " + insertId, null, null, null, null);
		cursor.moveToFirst();
		
		Course c = cursorToCourse(cursor);
		return c;
	}

	public void emptyTable(){
		database.execSQL("DELETE from " + SQLiteHelper.TABLE_COURSE);
	}

	/*public void deleteComment(Comment comment) {
		long id = comment.getId();
		System.out.println("Comment deleted with id: " + id);
		database.delete(SQLiteHelper.TABLE_COMMENTS, SQLiteHelper.COLUMN_ID
				+ " = " + id, null);
	}*/
	
	public List<Course> getAllCourses(){
		List<Course> courses = new ArrayList<Course>();
		
		Cursor cursor = database.query(SQLiteHelper.TABLE_COURSE, allColumns,
				null, null, null, null, null);
		
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Course course = cursorToCourse(cursor);
			courses.add(course);
			cursor.moveToNext();
		}
		
		cursor.close();
		return courses;
	}
	
	private Course cursorToCourse(Cursor cursor){
		Course c = new Course();
		c.setId(cursor.getLong(0));
		c.setCourse(cursor.getString(1));
		c.setRoom(cursor.getString(2));
		c.setTime(cursor.getString(3));
		c.setBeginHour(cursor.getInt(4));
		c.setEndHour(cursor.getInt(5));
		c.setDay(cursor.getString(6));
		
		return c;
	}

}

package be.pxl.planner.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import be.pxl.planner.model.Task;

public class TaskDataSource {

	private SQLiteDatabase database;
	private SQLiteHelper dbHelper;	
	private String [] allColumns_Task = { SQLiteHelper.TASK_ID, SQLiteHelper.TASK_NAME, SQLiteHelper.TASK_DESCRIPTION,
			SQLiteHelper.TASK_DEADLINE, SQLiteHelper.TASK_COURSE, SQLiteHelper.TASK_PERCENTAGE};

	public TaskDataSource(Context context) {
		dbHelper = new SQLiteHelper(context, null);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}
	
	
	public Task createTask (String name, String description, long deadline, String course, int percentage) {
		ContentValues values = new ContentValues();
		
		values.put(SQLiteHelper.TASK_NAME, name);
		values.put(SQLiteHelper.TASK_DESCRIPTION, description);
		values.put(SQLiteHelper.TASK_DEADLINE, deadline);
		values.put(SQLiteHelper.TASK_COURSE, course);
		values.put(SQLiteHelper.TASK_PERCENTAGE, percentage);
		
		long insertID = database.insert(SQLiteHelper.TABLE_TASK, null, values);
		Cursor cursor = database.query(SQLiteHelper.TABLE_TASK, allColumns_Task, SQLiteHelper.TASK_ID + " = " + insertID, null, null, null, null);
		cursor.moveToFirst();
		
		Task t = cursorToTask(cursor);
		return t;
	}

	public void emptyTable(){
		database.execSQL("DELETE from " + SQLiteHelper.TABLE_TASK);
	}

	public void deleteTask(Task task) {
		long id = task.getId();
		System.out.println("Comment deleted with id: " + id);
		database.delete(SQLiteHelper.TABLE_TASK, SQLiteHelper.TASK_ID + " = " + id, null);
	}
	
	
	public List<Task> getAllTasks() {
		List <Task> tasks = new ArrayList<Task>();
		
		Cursor cursor = database.query(SQLiteHelper.TABLE_TASK, allColumns_Task, null, null, null, null, null);
		cursor.moveToFirst();
		
		while(!cursor.isAfterLast()) {
			Task task = cursorToTask(cursor);
			tasks.add(task);
			cursor.moveToNext();
		}
		
		cursor.close();
		return tasks;
	}
	
	public Task getTask(long id) {
		Task task;
	
		String where = SQLiteHelper.TASK_ID + " = " + id;
		Cursor cursor = database.query(SQLiteHelper.TABLE_TASK, allColumns_Task, where, null, null, null, null);
		cursor.moveToFirst();
		task = cursorToTask(cursor);
		cursor.close();
		
		return task;
	}
	
	public void updateTask(long id, String name, String description,  String course, int percentage, long deadline ) {
		Task task = getTask(id);
		
		ContentValues values = new ContentValues();
		
		values.put(SQLiteHelper.TASK_NAME, name);
		values.put(SQLiteHelper.TASK_DESCRIPTION, description);
		values.put(SQLiteHelper.TASK_DEADLINE, deadline);
		values.put(SQLiteHelper.TASK_COURSE, course);
		values.put(SQLiteHelper.TASK_PERCENTAGE, percentage);
		
		String where = SQLiteHelper.TASK_ID + " = " + id;
		database.update(SQLiteHelper.TABLE_TASK, values, where, null);
		
	}
	
	private Task cursorToTask(Cursor cursor) {
		Task t = new Task();
		
		t.setId(cursor.getLong(0));
		t.setName(cursor.getString(1));
		t.setDescription(cursor.getString(2));
		t.setDeadline(cursor.getString(3));
		t.setCourse(cursor.getString(4));
		t.setPercentage(cursor.getInt(5));
		
		return t;		
	}

}

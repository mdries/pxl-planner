package be.pxl.planner.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class CateringRestClient {

	private final String BASE_URL;
	private static CateringRestClient crl = null;

	private CateringRestClient() {
		this.BASE_URL = "http://pxlcatering.mitchdries.cloudbees.net/webresources/catering/?campus=elfdelinie";
	}

	public static CateringRestClient getInstance() {
		if (crl == null) {
			crl = new CateringRestClient();
		}
		return crl;
	}

	public HashMap<String, String> getMenus() {
		StringBuilder builder = new StringBuilder();
		HttpClient client = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(BASE_URL);
		try {
			HttpResponse response = client.execute(httpGet);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) {
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
				Gson gson = new Gson();

				Type stringStringMap = new TypeToken<HashMap<String, String>>() {
				}.getType();
				HashMap<String, String> data = gson.fromJson(
						builder.toString(), stringStringMap);
				return data;
			} else {
				Log.e(CateringRestClient.class.toString(),
						"Failed to download file");
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}

package be.pxl.planner.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.util.Log;
import be.pxl.planner.app.DashboardActivity;
import be.pxl.planner.model.Day;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class TimetableRestClient {

	private final String BASE_URL;
	private static TimetableRestClient crl = null;

	private TimetableRestClient() {
		//10.0.2.2 is the localhost address. If you type localhost this will be the localhost of the emulator.
		this.BASE_URL = "http://pxlplannerschedule.mitchdries.cloudbees.net/webresources/timetable";
	}
	
	public static TimetableRestClient getInstance() {
		if(crl == null) {
			crl = new TimetableRestClient();
		}
		return crl;
	}

	public List<Day> getTimetable(String values) {		
		StringBuilder builder = new StringBuilder();
		HttpClient client = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(BASE_URL + values);
		try {
			HttpResponse response = client.execute(httpGet);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) {
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
				Gson gson = new Gson();
				Type collectionType = new TypeToken<Collection<Day>>() {
				}.getType();
				Collection<Day> contacts = gson.fromJson(
						builder.toString(), collectionType);
				List<Day> cList = new ArrayList<Day>(contacts);
				return cList;
			} else {
				Log.e(DashboardActivity.class.toString(),"Failed to download file");
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}

package be.pxl.planner;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import be.pxl.planner.model.Day;

public class __DayListAdapter extends ArrayAdapter<Day> {

	public __DayListAdapter(Context context, List<Day> list2) {
		super(context, R.layout.day_row_layout, list2);
		this.list = list2;
	}

	private List<Day> list;
	
	private static class ViewHolder {
		public TextView tv_test;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		if(rowView == null){
			LayoutInflater infl = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = infl.inflate(R.layout.day_row_layout, parent, false);
			ViewHolder vh = new ViewHolder();
			vh.tv_test = (TextView) rowView.findViewById(R.id.tvTest);
			rowView.setTag(vh);
		}
		
		ViewHolder vh2 = (ViewHolder) rowView.getTag();
		
		// DEZE REGEL GEEFT EEN FOUT!!!!!!
		
		Day d = list.get(position);
		vh2.tv_test.setText(d.getDay() + " aantal vakken: " + d.getClasses().size());
		return rowView;
	}
}

package be.pxl.planner.app;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import be.pxl.planner.database.CourseDataSource;
import be.pxl.planner.model.Course;

public class ScheduleActivity extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Controleren of er lessen toegevoegd zijn. Zoniet, roep dan ScheduleChooserActivity op.
		CourseDataSource datasource = new CourseDataSource(this);
	    datasource.open();
	    List<Course> values = datasource.getAllCourses();
	    Intent intent;
	    if(values.size() > 0){
	    	intent = new Intent(this, ScheduleViewActivity.class);
	    } else {
	    	intent = new Intent(this, ScheduleChooserActivity.class);
	    }
	    startActivity(intent);
	    
	    // Finish de activity
	    finish();
	}
}

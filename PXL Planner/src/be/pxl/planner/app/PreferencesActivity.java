package be.pxl.planner.app;

import java.io.File;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import be.pxl.planner.R;

public class PreferencesActivity extends Activity {

	private Button preferences_reset;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_preferences);
		
		preferences_reset = (Button) findViewById(R.id.preferences_reset);
		
		preferences_reset.setOnClickListener(new ButtonHandler());
	}
	
	
	private class ButtonHandler implements OnClickListener {
		@Override
		public void onClick(View arg0) {
			Context context = getApplicationContext();
			
			clearApplicationData(context);
			
			Toast msg = Toast.makeText(getApplicationContext(),"Je taken zijn verwijderd en je kan opnieuw je klas kiezen", Toast.LENGTH_SHORT);				
			msg.show();		
			finish();
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.preferences, menu);
		return true;
	}
	
	public static void clearApplicationData(Context context) {
	    File cache = context.getCacheDir();
	    File appDir = new File(cache.getParent());
	    if (appDir.exists()) {
	        String[] children = appDir.list();
	        for (String s : children) {
	            File f = new File(appDir, s);
	            if(deleteDir(f))
	                Log.i("", String.format("**************** DELETED -> (%s) *******************", f.getAbsolutePath()));
	        }
	    }
	}
	private static boolean deleteDir(File dir) {
	    if (dir != null && dir.isDirectory()) {
	        String[] children = dir.list();
	        for (int i = 0; i < children.length; i++) {
	            boolean success = deleteDir(new File(dir, children[i]));
	            if (!success) {
	                return false;
	            }
	        }
	    }
	    return dir.delete();
	}
}

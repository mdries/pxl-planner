package be.pxl.planner.app;

import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;
import be.pxl.planner.R;
import be.pxl.planner.client.TimetableRestClient;
import be.pxl.planner.database.CourseDataSource;
import be.pxl.planner.model.Course;
import be.pxl.planner.model.Day;

public class ScheduleChooserActivity extends Activity {
	private Spinner classes, studies;
	private Button proceed;

	private String args = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_schedule_chooser);

		classes = (Spinner) findViewById(R.id.spinner_classes);
		studies = (Spinner) findViewById(R.id.spinner_courses);
		proceed = (Button) findViewById(R.id.choose_schedule);

		// Studies is een spinner met de mogelijke studierichtingen.
		// Wanneer een item geselecteerd is zijn alle bijhorende klassen
		// zichtbaar in de 2de spinner.
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				this, R.array.classes, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		studies.setAdapter(adapter);
		studies.setOnItemSelectedListener(new SpinnerHandler());

		// Knop om verder te gaan
		proceed.setOnClickListener(new ProceedButtonHandler());
	}

	private class SpinnerHandler implements OnItemSelectedListener {
		@Override
		public void onItemSelected(AdapterView<?> parent, View v, int position,long id) {
			// Haalt de juiste string-array op die de klassen bevat.
			int[] arrayIds = { R.array.classes_tin, R.array.classes_marketing,
					R.array.classes_acf, R.array.classes_log,
					R.array.classes_rpr };

			ArrayAdapter<CharSequence> adapter = ArrayAdapter
					.createFromResource(ScheduleChooserActivity.this,
							arrayIds[position],
							android.R.layout.simple_spinner_item);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			classes.setAdapter(adapter);
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
		}
	}

	private class ProceedButtonHandler implements OnClickListener {

		@Override
		public void onClick(View v) {
			// Selected course
			String course = classes.getSelectedItem().toString();

			// Toon melding indien er geen internet is, anders achtergrondproces
			// voor het ophalen van het uurrooster starten.
			if (isOnline()) {
				args = "?group=" + course + "&department=auto";
				new AsyncLvLoader().execute();
			} else {
				Toast msg = Toast.makeText(getApplicationContext(),
						"Maak verbinding met internet om verder te gaan.",
						Toast.LENGTH_SHORT);
				msg.show();
			}

		}

	}

	private class AsyncLvLoader extends AsyncTask<String, Void, List<Day>> {
		
		private final ProgressDialog dialog = new ProgressDialog(
				ScheduleChooserActivity.this);
		
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Progress indicator tonen
			dialog.setMessage("Uurrooster ophalen...");
			dialog.setIndeterminate(false);
			dialog.show();
		}

		@Override
		protected void onPostExecute(List<Day> result) {
			super.onPostExecute(result);
			dialog.dismiss();

			// Lessen in databank plaatsen
			CourseDataSource datasource = new CourseDataSource(
					ScheduleChooserActivity.this);
			datasource.open();
			datasource.emptyTable();
			for (Day d : result) {
				for (Course c : d.getClasses()) {
					datasource.createCourse(c.getCourse(), c.getRoom(),
							c.getTime(), c.getBeginHour(), c.getEndHour(),
							Integer.parseInt(d.getDay()));
				}
			}
			datasource.close();

			// ScheduleViewActivity runnen en deze activity finishen
			Intent intent = new Intent(getApplicationContext(),
					ScheduleViewActivity.class);
			startActivity(intent);
			finish();

		}

		@Override
		protected List<Day> doInBackground(String... params) {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			// Haal de List<Day> op die het lesrooster representeert
			return TimetableRestClient.getInstance().getTimetable(args);
		}

	}

	// Kijkt of de gebruiker internettoegang heeft
	// http://stackoverflow.com/questions/1560788/how-to-check-internet-access-on-android-inetaddress-never-timeouts
	public boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

}

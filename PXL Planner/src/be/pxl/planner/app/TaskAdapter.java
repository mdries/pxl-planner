package be.pxl.planner.app;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import be.pxl.planner.R;
import be.pxl.planner.model.Task;

public class TaskAdapter extends ArrayAdapter<Task> {
	private List<Task> tasks;
	
	public TaskAdapter(Context context, List<Task> objects) {
		super(context, R.layout.task_row_layout, objects);
		this.tasks = objects;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		if(rowView == null){
			LayoutInflater infl = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rowView = infl.inflate(R.layout.task_row_layout, parent, false);
			ViewHolder newHolder = new ViewHolder();
			newHolder.name = (TextView) rowView.findViewById(R.id.task_title);
			newHolder.percentage = (TextView) rowView.findViewById(R.id.task_percentage);
			
			
			rowView.setTag(newHolder);
		}
		
		ViewHolder holder = (ViewHolder) rowView.getTag();
		Task task = tasks.get(position);
		
		holder.name.setText(task.getName());
		holder.percentage.setText(task.getPercentage() + "%");
		
		return rowView;
	}
	
	private static class ViewHolder{
		public TextView name;
		public TextView percentage;
	}

}

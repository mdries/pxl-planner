package be.pxl.planner.app;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import be.pxl.planner.R;

public class DashboardActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dashboard_activity);
		
		int[] buttonIds = { R.id.btn_Dagmenu, R.id.btn_Instellingen, R.id.btn_Taak_Toevoegen, R.id.btn_Taken, R.id.btn_Uurrooster };
		List<Button> buttons = new ArrayList<Button>();
		for (int id : buttonIds){
			Button button = (Button) findViewById(id);
			buttons.add(button);
			button.setOnClickListener(new ButtonHandler());
		}
	}
	
	private class ButtonHandler implements OnClickListener {		
		@Override
		public void onClick(View v) {
			Intent intent = null;
			switch(v.getId()){
				case R.id.btn_Uurrooster :
					intent = new Intent(getApplicationContext(), ScheduleActivity.class);
					break;
				case R.id.btn_Taak_Toevoegen:
					intent = new Intent(getApplicationContext(), NewTaskActivity.class);
					break;
				case R.id.btn_Dagmenu:
					intent = new Intent(getApplicationContext(), CateringActivity.class);
					break;
				case R.id.btn_Taken:
					intent = new Intent(getApplicationContext(), ShowTasksActivity.class);
					break;
				case R.id.btn_Instellingen:
					intent = new Intent(getApplicationContext(), PreferencesActivity.class);
			}
			
			if(intent != null){
				startActivity(intent);
			}
			
		}
		
	}


}

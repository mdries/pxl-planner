package be.pxl.planner.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import be.pxl.planner.R;

public class CateringDetailActivity extends Activity {
	public final static String TITLE = "title";
	public static final String DETAILS = "details";
	
	private TextView titleTv, detailsTv;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.catering_day_detail);
		
		titleTv = (TextView) findViewById(R.id.catering_day);
		detailsTv = (TextView) findViewById(R.id.catering_details);
		
		String title, details;
		Intent intent = getIntent();
		title = intent.getStringExtra(TITLE);
		details = intent.getStringExtra(DETAILS);
		
		titleTv.setText("Dagmenu van " + title);
		detailsTv.setText(details);
		
	}
}

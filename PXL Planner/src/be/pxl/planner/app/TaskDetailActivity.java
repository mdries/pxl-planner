package be.pxl.planner.app;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import be.pxl.planner.R;
import be.pxl.planner.database.TaskDataSource;
import be.pxl.planner.model.Task;

public class TaskDetailActivity extends Activity {
	public static final String TASK_ID = "task_id";
	
	private TextView task_detail_taskName, task_detail_courseName, task_detail_deadline, task_detail_description, task_detail_percentage_tekst;
	private SeekBar task_detail_percentage;
	private Button task_detail_edit, task_detail_delete;
	
	private TaskDataSource datasource;
	private Task task;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_task_detail);
		
		Intent intent = getIntent();
		long id = intent.getLongExtra(TASK_ID, 1);
		
		datasource = new TaskDataSource(TaskDetailActivity.this);
		datasource.open();
		task = datasource.getTask(id);
		
		
		task_detail_taskName = (TextView) findViewById(R.id.task_detail_taskName);
		task_detail_courseName = (TextView) findViewById(R.id.task_detail_courseName);
		task_detail_deadline = (TextView) findViewById(R.id.task_detail_deadline);
		task_detail_description = (TextView) findViewById(R.id.task_detail_description);
		task_detail_percentage_tekst = (TextView) findViewById(R.id.task_detail_percentage_tekst);
		task_detail_edit = (Button) findViewById(R.id.task_detail_edit);
		task_detail_delete = (Button) findViewById(R.id.task_detail_delete);
		
		task_detail_taskName.setText(task.getName());
		task_detail_courseName.setText(task.getCourse());
		task_detail_description.setText(task.getDescription());
		
		// Leesbare datum maken
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(Long.parseLong(task.getDeadline()));
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String date = dateFormat.format(cal.getTime());
		task_detail_deadline.setText(date);
		
		task_detail_percentage_tekst.setText(task.getPercentage() + "% voltooid");
				
		task_detail_delete.setOnClickListener(new ButtonHandler());
		task_detail_edit.setOnClickListener(new ButtonHandler());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.task_detail, menu);
		return true;
	}
	
	private class ButtonHandler implements OnClickListener {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.task_detail_delete:
				
				datasource.deleteTask(task);
				
				Toast msg = Toast.makeText(getApplicationContext(),"Je taak is verwijderd", Toast.LENGTH_SHORT);				
				msg.show();		
				
				datasource.close();
				Intent intentTasks = new Intent(TaskDetailActivity.this, ShowTasksActivity.class);
				intentTasks.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intentTasks);
				break;
			case R.id.task_detail_edit:
				
				Intent intent = new Intent(TaskDetailActivity.this, EditTaskActivity.class);
				intent.putExtra(EditTaskActivity.TASK_ID, task.getId());
				
				startActivity(intent);
				
				datasource.close();
			}
		
			
		}
		
	}

}

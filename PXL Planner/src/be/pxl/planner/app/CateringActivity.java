package be.pxl.planner.app;

import java.util.HashMap;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import be.pxl.planner.client.CateringRestClient;

public class CateringActivity extends ListActivity {
	private ArrayAdapter<String> adapter;
	private static HashMap<String, String> result;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		new AsyncCateringLoader().execute();
	}
	
	private class AsyncCateringLoader extends AsyncTask<String, Void, HashMap<String, String>>{
		private final ProgressDialog dialog = new ProgressDialog(CateringActivity.this);
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Progress indicator tonen
			dialog.setMessage("Dagmenu's ophalen...");
			dialog.setIndeterminate(false);
			dialog.show();
		}

		@Override
		protected void onPostExecute(HashMap<String, String> result) {
			super.onPostExecute(result);
			dialog.dismiss();
			
			if(result != null){
				CateringActivity.result = result;
				String[] dates = result.keySet().toArray(new String[result.keySet().size()]);
				adapter = new ArrayAdapter<String>(CateringActivity.this, android.R.layout.simple_list_item_1, dates);
				setListAdapter(adapter);
			} else {
				Toast msg = Toast.makeText(CateringActivity.this, "Er zijn nog geen dagmenu's gepubliceerd", Toast.LENGTH_SHORT);
				msg.show();
			}
			

		}
		
		@Override
		protected HashMap<String, String> doInBackground(String... params) {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			return CateringRestClient.getInstance().getMenus();
		}
		
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		Intent intent = new Intent(CateringActivity.this, CateringDetailActivity.class);
		String key = l.getItemAtPosition(position).toString();
		intent.putExtra(CateringDetailActivity.TITLE, key);
		intent.putExtra(CateringDetailActivity.DETAILS, result.get(key).toString());
		startActivity(intent);
	}

}

package be.pxl.planner.app;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import be.pxl.planner.R;
import be.pxl.planner.database.CourseDataSource;
import be.pxl.planner.database.TaskDataSource;
import be.pxl.planner.model.Course;
import be.pxl.planner.model.Task;

public class NewTaskActivity extends Activity {
	// Controls
	private EditText new_task_name, new_task_description;
	private Spinner new_task_course;
	private SeekBar new_task_percentage;
	private Button new_task_taak_Toevoegen;
	private Button datepicker_button;
	private TextView datepicker_textview;

	// Courses
	private ArrayList<Course> courses;
	private String selectedCourse;
	private CourseDataSource coursedatasource;

	// Tasks
	private TaskDataSource datasource;

	// Calendar variables
	private int year, month, day;
	private boolean date_is_set;
	private int mYear, mMonth, mDay;

	public NewTaskActivity() {
		final Calendar c = Calendar.getInstance();
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);

		// Standaard false, gebruikt voor validaties
		date_is_set = false;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.new_task_activity);

		new_task_name = (EditText) findViewById(R.id.new_task_name);
		new_task_description = (EditText) findViewById(R.id.new_task_description);
		new_task_course = (Spinner) findViewById(R.id.new_task_courses);
		new_task_percentage = (SeekBar) findViewById(R.id.new_task_percentage);
		new_task_taak_Toevoegen = (Button) findViewById(R.id.new_task_taaktoevoegen);
		datepicker_button = (Button) findViewById(R.id.datepicker_button);
		datepicker_textview = (TextView) findViewById(R.id.date_textview);
		
		datasource = new TaskDataSource(NewTaskActivity.this);
		datasource.open();
		new_task_taak_Toevoegen.setOnClickListener(new ButtonHandler());
		datepicker_button.setOnClickListener(new DatePickerHandler());

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
		coursedatasource = new CourseDataSource(NewTaskActivity.this);
		coursedatasource.open();
		courses = (ArrayList<Course>) coursedatasource.getAllCourses();

		// Toon melding dat vakken toegevoegd moeten worden
		if (courses.size() <= 0) {
			Toast msg = Toast.makeText(getApplicationContext(),
					"Er zijn geen vakken! Haal eerst je lesrooster op",
					Toast.LENGTH_SHORT);
			msg.show();
			finish();
		}
		
		for (Course c : courses) {
			// TODO check of er al course bestaat met zelfde naam
			adapter.add(c.getCourse());
		}
		
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		new_task_course.setAdapter(adapter);
		new_task_course.setOnItemSelectedListener(new CourseSpinnerHandler());
		coursedatasource.close();

		
	}

	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int yearSelected,
				int monthOfYear, int dayOfMonth) {
			year = yearSelected;
			month = monthOfYear;
			day = dayOfMonth;
			datepicker_textview.setText(day + "-" + month + "-" + year);
			date_is_set = true;
		}
	};

	private class DatePickerHandler implements OnClickListener {

		@Override
		public void onClick(View v) {
			DatePickerDialog dialog = new DatePickerDialog(
					NewTaskActivity.this, mDateSetListener, mYear, mMonth, mDay);
			dialog.show();
		}

	}

	private class CourseSpinnerHandler implements OnItemSelectedListener {

		@Override
		public void onItemSelected(AdapterView<?> av, View v, int position,
				long id) {
			// TODO mss beter om ID te gebruiken?
			selectedCourse = courses.get(position).getCourse();
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.new_task, menu);
		return true;
	}

	private class ButtonHandler implements OnClickListener {

		@Override
		public void onClick(View v) {

			Task task = null;
			switch (v.getId()) {
			case R.id.new_task_taaktoevoegen:

				if (!date_is_set) {
					Toast msg = Toast.makeText(getApplicationContext(),
							"Selecteer de datum van de deadline",
							Toast.LENGTH_SHORT);
					msg.show();
					break;
				}

				String name = new_task_name.getText().toString();
				String description = new_task_description.getText().toString();
				String course = selectedCourse;
				int percentage = new_task_percentage.getProgress();

				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DAY_OF_MONTH, day);
				cal.add(Calendar.MONTH, month);
				cal.add(Calendar.YEAR, year);

				long deadline = cal.getTime().getTime();

				if (name.equals("")) {
					Toast msg = Toast.makeText(getApplicationContext(),"Je moet wel iets ingeven!", Toast.LENGTH_SHORT);
					msg.show();
				} else {

					task = datasource.createTask(name, description, deadline, course, percentage);
					Toast msg = Toast.makeText(getApplicationContext(),"Je taak is opgeslagen", Toast.LENGTH_SHORT);
					msg.show();
					finish();
				}
				break;

			}
		}
	}

	@Override
	protected void onResume() {
		datasource.open();
		super.onResume();
	}

	@Override
	protected void onPause() {
		datasource.close();
		super.onPause();
	}

}

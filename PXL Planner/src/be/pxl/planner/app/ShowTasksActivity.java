package be.pxl.planner.app;

import java.util.ArrayList;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import be.pxl.planner.R;
import be.pxl.planner.database.TaskDataSource;
import be.pxl.planner.model.Task;

public class ShowTasksActivity extends ListActivity {

	private TaskDataSource datasource;
	private ArrayList<Task> courses;
	private TaskAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_tasks);
		
		TaskDataSource tds = new TaskDataSource(ShowTasksActivity.this);
		tds.open();
		courses = (ArrayList<Task>) tds.getAllTasks();
		
		adapter = new TaskAdapter(ShowTasksActivity.this, courses);
		setListAdapter(adapter);
		
		tds.close();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.show_tasks, menu);
		return true;		
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		
		Intent intent = new Intent(ShowTasksActivity.this, TaskDetailActivity.class);
		Task task = (Task) l.getItemAtPosition(position);		
		intent.putExtra(TaskDetailActivity.TASK_ID, task.getId());		
		startActivity(intent);
	}	  
}

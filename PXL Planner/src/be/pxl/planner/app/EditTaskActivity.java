package be.pxl.planner.app;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import be.pxl.planner.R;
import be.pxl.planner.database.TaskDataSource;
import be.pxl.planner.model.Task;

public class EditTaskActivity extends Activity {

	// Controls
	private EditText edit_task_name, edit_task_description;
	private SeekBar edit_task_percentage;
	private Button edit_task_taak_Toevoegen;
	private Button edit_task_datepicker_button;
	private TextView edit_task_datepicker_textview, edit_task_course;
	
	public static final String TASK_ID = "task_id";
	private Task task;
	private long id;
	
	
	// Tasks
	private TaskDataSource datasource;
	private String course;

	// Calendar variables
	private int year, month, day;
	private boolean date_is_set;
	private int mYear, mMonth, mDay;

	
	public EditTaskActivity() {
		final Calendar c = Calendar.getInstance();
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);

		// Standaard false, gebruikt voor validaties
		date_is_set = false;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_task);
		
		//id van de geselecteerde task ophalen
		Intent intent = getIntent();
		id = intent.getLongExtra(TASK_ID, 1);
		
		//de geselecteerde task ophalen
		datasource = new TaskDataSource(EditTaskActivity.this);
		datasource.open();
		task = datasource.getTask(id);
		
		edit_task_name = (EditText) findViewById(R.id.edit_task_name);
		edit_task_description = (EditText) findViewById(R.id.edit_task_description);
		edit_task_course = (TextView) findViewById(R.id.edit_task_courses);
		edit_task_percentage = (SeekBar) findViewById(R.id.edit_task_percentage);
		edit_task_taak_Toevoegen = (Button) findViewById(R.id.edit_task_taaktoevoegen);
		edit_task_datepicker_button = (Button) findViewById(R.id.edit_task_datepicker_button);
		edit_task_datepicker_textview = (TextView) findViewById(R.id.edit_task_date_textview);
		

		datasource = new TaskDataSource(EditTaskActivity.this);
		datasource.open();
		edit_task_taak_Toevoegen.setOnClickListener(new ButtonHandler());
		edit_task_datepicker_button.setOnClickListener(new DatePickerHandler());
		
		
		//de waardes al invullen
		course = task.getCourse(); //deze waarde al ophalen omdat deze niet veranderd
		edit_task_name.setText(task.getName());
		edit_task_description.setText(task.getDescription());
		edit_task_course.setText(task.getCourse());
		
		
		// Leesbare datum maken
		//edit_task_datepicker_textview.setText(task.getDeadline());
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(Long.parseLong(task.getDeadline()));
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String date = dateFormat.format(cal.getTime());
		edit_task_datepicker_textview.setText(date);
		
		edit_task_percentage.setProgress(task.getPercentage());
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.edit_task, menu);
		return true;
	}
	
	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int yearSelected, int monthOfYear, int dayOfMonth) {
			year = yearSelected;
			month = monthOfYear;
			day = dayOfMonth;
			edit_task_datepicker_textview.setText(day + "-" + month + "-" + year);
			date_is_set = true;
		}
	};

	private class DatePickerHandler implements OnClickListener {

		@Override
		public void onClick(View v) {
			DatePickerDialog dialog = new DatePickerDialog(EditTaskActivity.this, mDateSetListener, mYear, mMonth, mDay);
			dialog.show();
		}
	}
	
	private class ButtonHandler implements OnClickListener {

		@Override
		public void onClick(View v) {

			Task task = null;
			switch (v.getId()) {
			case R.id.edit_task_taaktoevoegen:

				if (!date_is_set) {
					Toast msg = Toast.makeText(getApplicationContext(),"Selecteer de datum van de deadline", Toast.LENGTH_SHORT);
					msg.show();
					break;
				}

				String name = edit_task_name.getText().toString();
				String description = edit_task_description.getText().toString();
				int percentage = edit_task_percentage.getProgress();

				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DAY_OF_MONTH, day);
				cal.add(Calendar.MONTH, month);
				cal.add(Calendar.YEAR, year);

				long deadline = cal.getTime().getTime();

				if (name.equals("")) {
					Toast msg = Toast.makeText(getApplicationContext(),"Je moet wel iets ingeven!", Toast.LENGTH_SHORT);
					msg.show();
				} else {

					
					datasource.updateTask(id, name, description, course, percentage, deadline);// adapter2.add(task);
					Toast msg = Toast.makeText(getApplicationContext(),"Je taak is opgeslagen", Toast.LENGTH_SHORT);
					msg.show();
					Intent intentTasks = new Intent(EditTaskActivity.this, ShowTasksActivity.class);
					intentTasks.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intentTasks);
				}
				break;

			}
		}
	}
}

package be.pxl.planner.app;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import be.pxl.planner.R;
import be.pxl.planner.database.CourseDataSource;
import be.pxl.planner.model.Course;

public class ScheduleViewActivity extends Activity {
	private List<RelativeLayout> dayLayouts;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.week_view_calendar);

		// In de zijbalk worden alle uren weergegeven
		LinearLayout scheduleRuler = (LinearLayout) findViewById(R.id.scheduleRuler);
		TextView hourTV;
		for (int i = 1; i <= 9; i++) { // Maximum 9 uren op een dag
			hourTV = new TextView(getApplicationContext());
			LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.MATCH_PARENT,
					LinearLayout.LayoutParams.WRAP_CONTENT);
			hourTV.setLayoutParams(layoutParams);
			hourTV.setHeight((int) convertDpToPixels(60)); // 60dp = 1u
			hourTV.setText((i + 7) + ".30");
			hourTV.setTextColor(Color.BLACK);
			hourTV.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
			scheduleRuler.addView(hourTV);
		}

		// Alle linear layouts representeren dagen, deze worden bijgehouden in
		// een ArrayList
		int dayIds[] = { R.id.day1, R.id.day2, R.id.day3, R.id.day4, R.id.day5 };
		dayLayouts = new ArrayList<RelativeLayout>();
		for (int id : dayIds) {
			dayLayouts.add((RelativeLayout) findViewById(id));
		}
		
		CourseDataSource datasource = new CourseDataSource(this);
	    datasource.open();
	    List<Course> values = datasource.getAllCourses();
	    for (Course course : values) {
	    	String text = course.getRoom() + "\n" + course.getCourse();
	    	addTextviewToCalendar(dayLayouts.get(Integer.parseInt(course.getDay())), course.getBeginHour(), 
	    			course.getEndHour(), Color.rgb(88, 166, 24), Color.WHITE, text);
		}
	    datasource.close();
	}
	
	private void addTextviewToCalendar(RelativeLayout day, int beginHour, int endHour, int backgroundColor, int textColor, String txt){
		TextView tv = new TextView(getApplicationContext());
		
		// Margins instellen
		RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		
		// Begin en duration berekenen (-8 voor zero-based)
		beginHour -= 8;
		endHour -= 8;
		
		int duration = endHour-beginHour;
		
		// Margin top = begin hour, height = duration
		layoutParams.setMargins(1, beginHour * (int)convertDpToPixels(60) -1, 1, 1);
		tv.setLayoutParams(layoutParams);
		tv.setHeight((int) (convertDpToPixels(duration*60)));
		tv.setBackgroundColor(backgroundColor);
		tv.setTextColor(textColor);
		tv.setText(txt);
		tv.setPadding(5, 5, 5, 5);
		day.addView(tv);
	}

	// Snippet
	// http://stackoverflow.com/questions/4605527/converting-pixels-to-dp
	private float convertDpToPixels(int dp) {
		// / Converts 14 dip into its equivalent px
		Resources r = getResources();
		float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
				r.getDisplayMetrics());
		px = Math.round(px * 100) / 100;
		return px;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.schedule, menu);
		return true;
	}
	

}

package be.pxl.planner.model;


public class Task {

	private long id;
	private String name, description, deadline, course;
	private int percentage;
	
	public Task() {		
	}
	
	public Task(String name, String description, String deadline, String course, int percentage) {
		super();
		this.name = name;
		this.description = description;
		this.deadline = deadline;
		this.course = course;
		this.percentage = percentage;
	}

	public int getPercentage() {
		return percentage;
	}

	public void setPercentage(int percentage) {
		this.percentage = percentage;
	}

	public String getName() {
		return name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDeadline() {
		return deadline;
	}

	public void setDeadline(String deadline) {
		this.deadline = deadline;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}
	
	public String toString() {
		return "" + name + "\n" + description + "\n" + deadline + ", " + course + " - " + percentage + "%";
	}
	
	
	
	
}

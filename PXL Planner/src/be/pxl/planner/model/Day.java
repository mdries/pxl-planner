package be.pxl.planner.model;

import java.util.ArrayList;

public class Day {
	private ArrayList<Course> courses;
	private String day;

	public Day() {
		this.courses = new ArrayList<Course>();
	}

	public Day(String day) {
		this.courses = new ArrayList<Course>();
		this.day = day;
	}

	public void addCourse(Course class1) {
		if (!this.courses.contains(class1)) {
			this.courses.add(class1);
		}
	}

	public ArrayList<Course> getClasses() {
		return this.courses;
	}

	public String getDay() {
		return this.day;
	}



}

package be.pxl.planner.model;

public class Class{
	
	private String course, room, time;
	private int beginHour, endHour;
	
	public Class(String course, String room, String time) {
		super();
		this.course = course;
		this.room = room;
		this.time = time;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	@Override
	public boolean equals(Object o) {
		Class class1 = this;
		Class class2 = (Class) o;
		boolean courseEquals, roomEquals, timeEquals;
		courseEquals = class1.course.equals(class2.course);
		roomEquals = class1.room.equals(class2.room);
		timeEquals = class1.time.equals(class2.time);
		return courseEquals && roomEquals && timeEquals;		
	}

	public int getBeginHour() {
		return beginHour;
	}

	public void setBeginHour(int beginHour) {
		this.beginHour = beginHour;
	}

	public int getEndHour() {
		return endHour;
	}

	public void setEndHour(int endHour) {
		this.endHour = endHour;
	}

}

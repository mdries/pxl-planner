import java.util.ArrayList;
import java.util.Iterator;

public class Day implements Iterable<Class> {
	private ArrayList<Class> classes;

	public Day() {
		this.classes = new ArrayList<Class>();
	}

	public void addClass(Class class1) {
		if (!this.classes.contains(class1)) {
			this.classes.add(class1);
		}
	}

	public ArrayList<Class> getClasses() {
		return this.classes;
	}

	@Override
	public Iterator<Class> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

}

import java.io.IOException;
import org.jsoup.*;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {		
		Document doc = null;
		try {
			doc = Jsoup.connect("http://www.pxl.be/Pub/Studenten/Voorzieningen-Student/Catering/Weekmenu-Campus-Elfde-Linie.html").get();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//ArrayList<Dagmenu> lijstDagmenus = new ArrayList<Dagmenu>();
		Elements dagmenus = doc.getElementsByClass("catering1");
		for(Element dagmenu : dagmenus){
			Element date = dagmenu.getElementsByClass("date").first();
			Elements subitems = dagmenu.getElementsByTag("p");
			System.out.println("\n" + date.html());
			for (Element element : subitems) {
				System.out.println(element.html());
			}
		}

	}

}

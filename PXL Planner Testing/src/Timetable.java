import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.*;
import org.jsoup.Connection.Response;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Timetable {

	public static final int REG_EX_HOUR = 1;
	public static final int REG_EX_TITLE = 2;
	public static final int REG_EX_ROOM = 3;
	
	public static void main(String[] args) {		
		String url = "https://kalender.pxl.be/kalenterit2/index.php?kt=lk&yks=&cluokka=3AOND&av=131021131028131021&guest=IT%2Fphl&lang=fla&print=netti&outmode=excel_inline";
		Connection con = Jsoup.connect(url);
		con.userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:5.0) Gecko/20100101 Firefox/5.0");
		
		Response response1 = null;
		Document doc = null;
		
		try {
			response1 = con.execute();
			doc = response1.parse();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Day[] lesrooster = new Day[5];
		for(int i = 0; i < lesrooster.length; i++){
			lesrooster[i] = new Day();
		}
		
		int currentDay = 0;
		
		Elements dagen = doc.getElementsByTag("tr"); //eerst de rijen eruit halen
		for(Element les : dagen){
			Elements th = les.getElementsByTag("td"); //uit iedere rij de kolom halen
			for (Element td : th) {
				currentDay++;
				Element span = td.getElementsByTag("span").first(); //per kolom de span eruit halen als er geen span is => null				
				if (span != null) {
					String gegevens = span.toString();
					String c = getGeg(gegevens, "(([01]?[0-9]|2[0-3]):[0-5][0-9])-(([01]?[0-9]|2[0-3]):[0-5][0-9])", REG_EX_HOUR); //de tijd ophalen
					String t = getGeg(gegevens, "</b>(.*)- ((HC)|(PR))", REG_EX_TITLE); //de titel ophalen
					String r = getGeg(gegevens, "((EB|EA|TE|TF)[0-9][0-9][0-9])", REG_EX_ROOM); //het klaslokaal ophalen
					System.out.println(c+"\n"+t+"\n"+r);
					
					lesrooster[currentDay-1].addClass(new Class(c, r, t));

				}
				//System.out.println(currentDay);
				System.out.println("--------------------------");
				
				if(currentDay == 5) {
					currentDay = 0;
				}
			}
		}
		
		System.out.println("finished");
		
		for (int i = 0; i < lesrooster.length; i++) {
			Day day = lesrooster[i];
			for(Class cl : day.getClasses()){
				System.out.println(cl.getCourse() + "\t" + cl.getRoom() + "\t" + cl.getTime());
			}
		}
		
		
	}
	
		
	//methode om de titel, het lokaal of de tijd uit de span te halen
	public static String getGeg(String title, String patternString, int nr) {
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(title);
		String result = null;
		if (nr == 2 || nr == 3) {
			if(matcher.find()) {
				result =  matcher.group(1);
			}	
		}
		else {		
			if(matcher.find()) {
				result = matcher.group(1) + " - " + matcher.group(3);
			 }
		}
		if(result == null){
			result = "";
		}
		return result;
	}

}